<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 06: Location Awareness</title>
</head>
<body>
<xmp>

# Lecture 06: Location Awareness

---

## Geolocation

* The **Geolocation API** extends the [**Geolocation web spec**](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation)

* The interface represents an object able to programmatically obtain the position of the device.

* It gives the app access to the location of the device.

* This allows an app to offer customized results based on the user's location.

### Requesting Permission

* On React Native for Android projects, we need to enable location by modifying the manifest file `android/app/src/main/AndroidManifest.xml` to request permission:

```
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
```

* In our code, we will need to request permission for `ACCESS_FINE_LOCATION` using the [**PermissionsAndroid API**](https://facebook.github.io/react-native/docs/permissionsandroid.html)

```javascript
import { PermissionsAndroid } from 'react-native';

async function requestLocationPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                'title': 'Geolocation Permission Required',
                'message': 'This app needs to access your device location',
            }
        )

        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('Location permission granted')
        }
        else {
            console.log('Location permission denied')
        }

        return granted
    }
    catch (err) {
        console.warn(err)
    }
}
```

* In the example above, we requested permission for `ACCESS_FINE_LOCATION`.

  * `ACCESS_FINE_LOCATION` provides better and accurate locations.

  * It determines position using both `GPS_PROVIDER` and `NETWORK_PROVIDER`
  
* Alternatively, we may request for the `ACCESS_COARSE_LOCATION`.

  * `ACCESS_COARSE_LOCATION` provides less accurate locations.
  
  * IT determines position using only `NETWORK_PROVIDER`.

  

### Get Current Position

* `Geolocation.getCurrentPosition()`: Determines the device's current location and gives back a `Position` object with the data.

```
navigator.geolocation.getCurrentPosition(
	(position) => this.setState({position}),
	(error) => console.log(error.message),
	{
		enableHighAccuracy: true,
		timeout: 20000,
		maximumAge: 1000
	}
);
```

* A sample `Position` object is as shown below:

```
{
	mocked: false,
	timestamp: 1532674683000,
	coords: {
		speed: 0,
		heading: 0,
		accuracy: 20,
		longitude: 101.5043,
		altitude: 0.1,
		latitude: 2.6794
	}
}
```

### Watch Position

* `Geolocation.watchPosition()`: Returns a `long` value representing the newly established callback function to be invoked whenever the device location changes.

```javascript
this.watchID = navigator.geolocation.watchPosition((position) => {
	this.setState({position});
});
```

### Clear Watch

* We should always clear the handler previously installed using `watchPosition()` when the component is unmounted.

```javascript
navigator.geolocation.clearWatch(this.watchID)
```

### Demonstration App

* This app displays the **latitude**, **longitude** & **altitude** based on geolocation information of the device:

```javascript
import React, { Component } from 'react';
import {
  PermissionsAndroid,
  StyleSheet,
  Text,
  View
} from 'react-native';

async function requestLocationPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                'title': 'Geolocation Permission Required',
                'message': 'This app needs to access your device location',
            }
        )

        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('Location permission granted')
        }
        else {
            console.log('Location permission denied')
        }

        return granted
    }
    catch (err) {
        console.warn(err)
    }
}


export default class App extends Component<Props> {
  constructor(props) {
      super(props)

      this.state = {
          granted: PermissionsAndroid.RESULTS.DENIED,
          position: null,
      };

      this.readLocation = this.readLocation.bind(this)
  }

 componentDidMount() {
     let granted = requestLocationPermission();

     this.setState({
         granted: granted,
     })

     if(granted) this.readLocation()
 }

 componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID)
 }

 readLocation() {
     navigator.geolocation.getCurrentPosition(
         (position) => this.setState({position}),
         (error) => console.log(error.message),
         {
             enableHighAccuracy: true,
             timeout: 20000,
             maximumAge: 1000
         }
     );

    this.watchID = navigator.geolocation.watchPosition((position) => {
      this.setState({position});
    });
 }


  render() {
    return (
      <View style={styles.container}>
        <View>
            <Text style={styles.label}>
              Longitude
            </Text>
            <Text style={styles.data}>
              { this.state.position ? this.state.position.coords.longitude : 'unknown' }
            </Text>
        </View>
        <View>
            <Text style={styles.label}>
              Latitude
            </Text>
            <Text style={styles.data}>
              { this.state.position ? this.state.position.coords.latitude : 'unknown' }
            </Text>
        </View>
        <View>
            <Text style={styles.label}>
              Altitude
            </Text>
            <Text style={styles.data}>
              { this.state.position ? this.state.position.coords.altitude : 'unknown' }
            </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  label: {
    fontSize: 18,
    textAlign: 'center',
    margin: 10,
  },
  data: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
  },
});
```

---

## Open Maps App Using Location Information

### Install react-native-open-maps

* Install the `react-native-open-maps` module:

```
npm install --save react-native-open-maps
```

### Usage

* Import:

```javascript
import openMap from 'react-native-open-maps'
```

* Call the `openMap` method and provide the coordinates (`longitude` and `latitude`) as an object:

```javascript
openMap({
	latitude: 2.6794,
	longitude: 101.5043,
})
```

### Sample App

```javascript
import React, { Component } from 'react';
import {
  PermissionsAndroid,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableNativeFeedback,
  View
} from 'react-native';
import openMap from 'react-native-open-maps';

async function requestLocationPermission() {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                'title': 'Geolocation Permission Required',
                'message': 'This app needs to access your device location',
            }
        )

        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('Location permission granted')
        }
        else {
            console.log('Location permission denied')
        }

        return granted
    }
    catch (err) {
        console.warn(err)
    }
}


export default class App extends Component<Props> {
  constructor(props) {
      super(props)

      this.state = {
          granted: PermissionsAndroid.RESULTS.DENIED,
          position: null,
      };

      this.readLocation = this.readLocation.bind(this)
      this.onPress = this.onPress.bind(this)
  }

 componentDidMount() {
     let granted = requestLocationPermission();

     this.setState({
         granted: granted,
     })

     if(granted) this.readLocation()
 }

 componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID)
 }

 readLocation() {
     navigator.geolocation.getCurrentPosition(
         (position) => this.setState({position}),
         (error) => console.log(error.message),
         {
             enableHighAccuracy: true,
             timeout: 20000,
             maximumAge: 1000
         }
     );

    this.watchID = navigator.geolocation.watchPosition((position) => {
      this.setState({position});
    });
 }

 onPress() {
     if(this.state.position) {
         openMap({
             latitude: this.state.position.coords.latitude,
             longitude: this.state.position.coords.longitude,
         })
     }
     else {
         ToastAndroid.show('Location is unknown!', ToastAndroid.LONG);
     }
 }

  render() {
    return (
      <View style={styles.container}>
        <View>
            <Text style={styles.label}>
              Longitude
            </Text>
            <Text style={styles.data}>
              { this.state.position ? this.state.position.coords.longitude : 'unknown' }
            </Text>
        </View>
        <View>
            <Text style={styles.label}>
              Latitude
            </Text>
            <Text style={styles.data}>
              { this.state.position ? this.state.position.coords.latitude : 'unknown' }
            </Text>
        </View>
        <View>
            <Text style={styles.label}>
              Altitude
            </Text>
            <Text style={styles.data}>
              { this.state.position ? this.state.position.coords.altitude : 'unknown' }
            </Text>
        </View>
        <TouchableNativeFeedback onPress={this.onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>
                    Open in Maps
                </Text>
            </View>
        </TouchableNativeFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  button: {
    backgroundColor: '#a80000',
  },
  buttonText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 20,
  },
  label: {
    fontSize: 18,
    textAlign: 'center',
    margin: 10,
  },
  data: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
  },
});
```

---

###### The contents in this page are generally based on React Native's Documentation at https://facebook.github.io/react-native/
</xmp> 
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>