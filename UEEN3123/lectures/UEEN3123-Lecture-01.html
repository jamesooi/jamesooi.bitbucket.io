<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 01: Introduction to Network Programming &amp; Sockets</title>
</head>
<body>
<xmp>

# Lecture 01: Introduction to Network Programming & Sockets

---

## TCP/IP Layered Model

* There are 4 layers in Internet model, from top to bottom:

  * *Application* layer
  
  * *Transport* layer
  
  * *Internet* layer

  * *Network Access* layer

<img src="images/tcpip-model.png" style="max-width: 640px">

---

## Why Layered Model?

* Dealing with complex systems

  * Explicit structure allows identification, relationship of complex system's pieces

  * Modularization eases maintenance, updating of system

    * change of implementation of layer's service transparent to rest of system

	* e.g.: change in underlying network infrastructure doesn't affect the rest of system

* Easy to swap in and out (upgrade) for each layer

* Special people trained for each layer

---

## Overview of Network Programming

* Network applications are widely used

  * Web, email, & even many of the mobile apps that we use daily (e.g. WhatsApp, WeChat, etc.)

  
* Network Programming involves writing computer programs that enable processes to communicate with each other across a computer network.

  * Interestingly, all network applications are based on the same basic programming model, have similar overall logical structures, and rely on the same programming interface.

---

## Client-Server Model

* Distributed application structure that partitions tasks or workloads between the providers of a resource or service, called *servers*, and service requesters, called *clients*

<img src="images/client-server.png" style="max-width: 640px">

* A server host runs one or more server programs which share their resources with clients.

  * A client does not share any of its resources, but requests a server's content or service function

---

## Peer-to-peer Model

* Distributed application architecture that partitions tasks or work loads between peers

* Peers are equally privileged, equipotent participants in the application. They are said to form a *peer-to-peer network* of nodes

* Peers make a portion of their resources, such as processing power, disk storage or network bandwidth, directly available to other network participants, without the need for central coordination by servers or stable hosts

* Peers are both suppliers and consumers of resources

<img src="images/p2p.png" style="max-width: 640px">

* A popular example of a peer-to-peer application is *blockchain*, which is the underlying technology that supports cryptocurrencies

<img src="images/blockchain.jpg" style="max-width: 640px">

---

## Sockets API

* Network socket

  * An endpoint of an inter-process communication across a computer network.

  * Today, most communication between computers is based on the Internet Protocol; therefore most network sockets are Internet sockets.

* Sockets API

  * An API that allows application programs to control and use network sockets.

  
* Socket address

  * Combination of an IP address and a port number.
  
  * Based on this address, internet sockets deliver incoming data packets to the appropriate application process or thread.

---

## Client-Server Communications using Sockets

* The server must be running when a client starts.

  * `socket()`: A socket is created.

  * `bind()`: The socket is bind to a TCP port number.

  * `listen()`: The server listens for a connection request from a client.

* The client connects to a running server.

  * `socket()`: A socket is created.
  
  * `connect()`: Client connects to the server, specifying the IP address & TCP port number.

* The server accepts the connection (`accept()`).

* Either side uses `send()` to send data and `recv()` to receive data.

* `close()`: Closes the connection. 

<img src="images/client-server-flow.jpg" style="max-width: 640px">

---

## A Simple Client-Server Program

### Server Program

```python
import socket
import math
import json

HOST = ''           # Symbolic name meaning all available interfaces
PORT = 8509         # The TCP port number this server is bound to

# Create a socket object, bind it to (HOST, PORT) &
# listen for incoming connection
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)  # use AF_INET for IPv4
s.bind((HOST, PORT))
s.listen()

# Infinite loop, each iteration handles a connection from a client
while True:
    # Accepts a connection
    conn, addr = s.accept()

    # Receive radius from client, decode the bytes to string
    # & convert it to float
    radius = float(conn.recv(1024).decode())

    # Calculate the circumference & area
    circum = 2 * math.pi * radius
    area = math.pi * radius * radius

    # Wrap the results into a dict
    response = {
        'circum': circum,
        'area': area,
    }

    # Serialize dict into JSON, encode into bytes & send to client
    conn.sendall(json.dumps(response).encode())
```

### Client Program

```python
import socket
import json

HOST = '::1'        # IPv6 address of the server
PORT = 8509         # The TCP port number of the server

# Create a socket object, connect to (HOST, PORT)
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)  # use AF_INET for IPv4
s.connect((HOST, PORT))

print('Circle calculator')

# Get the radius from the user interactively
radius = input('Radius: ')

# Encode the input into bytes & send to server
s.sendall(radius.encode())

# Receive the response from the server & convert the bytes (JSON string)
# into dict
response = json.loads(s.recv(1024))

# Print the result
print('Circumference: {}'.format(response['circum']))
print('Area: {}'.format(response['area']))

# Close the socket
s.close()
```

---

## Multithreaded Server

* Multiple clients are quite often connected to a single server at the same time.

* Typically, a server runs constantly on a server computer, and clients from all over the Internet may want to connect to it.

* You can use _threads_ to handle the server's multiple clients simultaneously.

  * Simply create a thread for each connection.
  
  
* Here is how the server handles the establishment of a connection:

```python
while True:
    # Accepts a connection
    conn, addr = s.accept()

    # Start thread
    t = ClientThread(conn, addr)
    t.start()
```

* The server socket can have many connections.

* Each iteration of the `while` loop creates a new connection.

* Whenever a connection is established, a new thread is created to handle communication between the server and the new client. This allows multiple connections to run at the same time.

### Server Program

```python
import socket
import threading
import math
import json

class ClientThread(threading.Thread):
    def __init__(self, conn, addr):
        threading.Thread.__init__(self)
        self.socket = conn
        self.addr = addr
        print('[+] New thread started for client {0}'.format(addr))

    def run(self):
        while True:
            # Receive radius from client, decode the bytes to string
            # & convert it to float
            radius = float(self.socket.recv(1024).decode())
            print('[*] Received data from client {0}, value {1}'.format(self.addr, radius))

            # Calculate the circumference & area
            circum = 2 * math.pi * radius
            area = math.pi * radius * radius

            # Wrap the results into a dict
            response = {
                'circum': circum,
                'area': area,
            }

            # Serialize dict into JSON, encode into bytes & send to client
            self.socket.sendall(json.dumps(response).encode())
            print('[*] Sent data to client {0}'.format(self.addr))


HOST = ''           # Symbolic name meaning all available interfaces
PORT = 8509         # The TCP port number this server is bound to

# Create a socket object, bind it to (HOST, PORT) &
# listen for incoming connection
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)  # use AF_INET for IPv4
s.bind((HOST, PORT))
s.listen()


# Infinite loop, each iteration handles a connection from a client
while True:
    # Accepts a connection
    conn, addr = s.accept()

    # Start thread
    t = ClientThread(conn, addr)
    t.start()
```

### Client Program

```python
import socket
import json

HOST = '::1'        # IPv6 address of the server
PORT = 8509         # The TCP port number of the server

# Create a socket object, connect to (HOST, PORT)
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)  # use AF_INET for IPv4
s.connect((HOST, PORT))

print('Circle calculator')

# Get the radius from the user interactively
radius = input('Radius (0 to Quit): ')

while radius != '0':
    # Encode the input into bytes & send to server
    s.sendall(radius.encode())

    # Receive the response from the server & convert the bytes (JSON string)
    # into dict
    response = json.loads(s.recv(1024))

    # Print the result
    print('Circumference: {}'.format(response['circum']))
    print('Area: {}'.format(response['area']))

    # Get the radius from the user interactively
    radius = input('Radius (0 to Quit): ')

# Close the socket
s.close()
```

---

</xmp> 
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
