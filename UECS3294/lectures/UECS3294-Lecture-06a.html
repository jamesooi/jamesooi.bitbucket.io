<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 06: Authentication &amp; Authorization</title>
</head>
<body>
<xmp theme="united" style="display:none;">

# Lecture 06 (A): Authentication

---

## Introduction

* Laravel makes implementing authentication very simple as almost everything is configured out of the box.

* The authentication configuration file is located at  `config/auth.php`, which contains several well documented options for tweaking the behavior of the authentication services.

* At its core, Laravel's authentication facilities are made up of _guards_ and _providers_.

### Guards

* Guards define how users are authenticated for each request. 

* For example, Laravel ships with a `session` guard which maintains state using session storage and cookies.

### Providers

* Providers define how users are retrieved from your persistent storage.

* Laravel ships with support for retrieving users using Eloquent and the database query builder.

* However, you are free to define additional providers as needed for your application.

---

## Database Considerations

* By default, Laravel includes an `App\User` Eloquent model in your `app` directory.

* This model may be used with the default Eloquent authentication driver.

* If your application is not using Eloquent, you may use the `database` authentication driver which uses the Laravel query builder.

* When building the database schema for the `App\User` model, make sure the `password` column is at least 60 characters in length.

* Verify that your `users` (or equivalent) table contains a nullable, string `remember_token` column of 100 characters. This column will be used to store a token for users that select the _remember me_ option when logging into your application.

---

## Authentication Quickstart

* Laravel ships with several pre-built authentication controllers, which are located in the  `App\Http\Controllers\Auth` namespace.

  * The `RegisterController` handles new user registration.
  
  * The `LoginController` handles authentication.
  
  * The `ForgotPasswordController` handles e-mailing links for resetting passwords.
  
  * The `ResetPasswordController` contains the logic to reset passwords.
  
* Each of these controllers uses a trait to include their necessary methods.

* For many applications, you will not need to modify these controllers at all.


### Routing

* Laravel provides a quick way to scaffold all of the routes and views you need for authentication using one simple command:

```php
php artisan make:auth
```

* This command should be used on fresh applications and will install a layout view, registration and login views, as well as routes for all authentication end-points.

* A `HomeController` will also be generated to handle post-login requests to your application's dashboard.


### Views

* The `make:auth` Artisan command will create all of the views you need for authentication and place them in the `resources/views/auth` directory.

* The `make:auth` command will also create a `resources/views/layouts` directory containing a base layout for your application. All of these views use the Bootstrap CSS framework, but you are free to customize them however you wish.


### Authenticating

* Now that you have routes and views setup for the included authentication controllers, you are ready to register and authenticate new users for your application!

* You may access your application in a browser since the authentication controllers already contain the logic (via their traits) to authenticate existing users and store new users in the database.


### Path Customization

* When a user is successfully authenticated, by default, they will be redirected to the `/home` URI.

* You can customize the post-authentication redirect location by defining a `redirectTo` property on the  `LoginController`, `RegisterController`, and `ResetPasswordController`:

```php
protected $redirectTo = '/';
```

* If the redirect path needs custom generation logic you may define a `redirectTo` method instead of a `redirectTo` property:

```php
protected function redirectTo()
{
    return '/path';
}
```

* The `redirectTo` method will take precedence over the `redirectTo` attribute.

### Username Customization

* By default, Laravel uses the `email` field for authentication.

* If you would like to customize this, you may define a `username` method on your `LoginController`:

```php
public function username()
{
    return 'username';
}
```

### Guard Customization

* You may also customize the _guard_ that is used to authenticate and register users.

* To get started, define a `guard` method on your `LoginController`, `RegisterController`, and  `ResetPasswordController`. The method should return a guard instance:

```php
use Illuminate\Support\Facades\Auth;

protected function guard()
{
    return Auth::guard('guard-name');
}
```

### Validation/Storage Customization

* To modify the form fields that are required when a new user registers with your application, or to customize how new users are stored into your database, you may modify the `RegisterController` class. This class is responsible for validating and creating new users of your application.

* The `validator` method of the `RegisterController` contains the validation rules for new users of the application. You are free to modify this method as you wish.

* The `create` method of the `RegisterController` is responsible for creating new `App\User` records in your database using the Eloquent ORM. You are free to modify this method according to the needs of your database.

### Retrieving The Authenticated User

* You may access the authenticated user via the `Auth` facade:

```php
use Illuminate\Support\Facades\Auth;

// Get the currently authenticated user...
$user = Auth::user();

// Get the currently authenticated user's ID...
$id = Auth::id();
```

* Alternatively, once a user is authenticated, you may access the authenticated user via an  `Illuminate\Http\Request` instance.

* Remember, type-hinted classes will automatically be injected into your controller methods:

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Update the user's profile.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(Request $request)
    {
        // $request->user() returns an instance of the authenticated user...
    }
}
```

### Determining If The Current User Is Authenticated

* To determine if the user is already logged into your application, you may use the `check` method on the `Auth` facade, which will return `true` if the user is authenticated:

```php
use Illuminate\Support\Facades\Auth;

if (Auth::check()) {
    // The user is logged in...
}
```

### Protecting Routes

* Even though it is possible to determine if a user is authenticated using the `check` method, you will typically use a _middleware_ to verify that the user is authenticated before allowing the user access to certain routes / controllers. 

* Route middleware can be used to only allow authenticated users to access a given route.

* Laravel ships with an `auth` middleware, which is defined at `Illuminate\Auth\Middleware\Authenticate`.

* Since this middleware is already registered in your HTTP kernel, all you need to do is attach the middleware to a route definition:

```php
Route::get('profile', function () {
    // Only authenticated users may enter...
})->middleware('auth');
```

* Of course, if you are using controllers, you may call the `middleware` method from the controller's constructor instead of attaching it in the route definition directly:

```php
public function __construct()
{
    $this->middleware('auth');
}
```

### Specifying A Guard

* When attaching the auth middleware to a route, you may also specify which guard should be used to authenticate the user.

* The guard specified should correspond to one of the keys in the `guards` array of your `auth.php` configuration file:

```php
public function __construct()
{
    $this->middleware('auth:api');
}
```

### Login Throttling

* If you are using Laravel's built-in `LoginController` class, the `Illuminate\Foundation\Auth\ThrottlesLogins` trait will already be included in your controller.

* By default, the user will not be able to login for one minute if they fail to provide the correct credentials after several attempts.

* The throttling is unique to the user's `username` / `e-mail` address and their IP address.


### Manually Authenticating Users

* Other than using the authentication controllers included with Laravel, you may choose to remove these controllers and manage user authentication using the Laravel authentication classes directly.

* We will access Laravel's authentication services via the `Auth` facade, so we'll need to make sure to import the `Auth` facade at the top of the class.

* Use the `attempt` method:

```php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate()
    {
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
    }
}
```

* The `attempt` method accepts an array of key / value pairs as its first argument.

  * You should not hash the `password` specified as the password value, since the framework will automatically hash the value before comparing it to the hashed password in the database.

* The `attempt` method will return `true` if authentication was successful. Otherwise, `false` will be returned.

* The `intended` method on the redirector will redirect the user to the URL they were attempting to access before being intercepted by the authentication middleware. 

#### Specifying Additional Conditions

* You may also add extra conditions to the authentication query in addition to the user's `email` and `password`. For example, we may verify that user is marked as _active_:

```php
if (Auth::attempt(['email' => $email, 'password' => $password, 'active' => 1])) {
    // The user is active, not suspended, and exists.
}
```

#### Logging Out

* To log users out of your application, you may use the `logout` method on the `Auth` facade. This will clear the authentication information in the user's session:

```php
Auth::logout();
```

#### Remembering Users

* If you would like to provide _remember me_ functionality in your application, you may pass a `boolean` value as the second argument to the `attempt method`, which will keep the user authenticated indefinitely, or until they manually logout.

* The `users` table must include the string `remember_token` column, which will be used to store the _remember me_ token.

```php
if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
    // The user is being remembered...
}
```

---

## API Authentication

* Although Laravel makes it easy for you to implement authentication almost out of the box, the generated code is only good for implementing authentication for traditional web apps.

* To implement authentication for Web API, we will need to use **Token-based Authentication**.

* In this class, you will learn how to set up JSON Web Tokens to use for API authentication.

---

## JSON Web Tokens (JWT)

* JSON Web Tokens (JWT) are an open, industry standard [RFC 7519](https://tools.ietf.org/html/rfc7519) method for representing claims securely between two parties.

### Setting Up JWT Package

* To use JWT in our Laravel application, we will use the [**jwt-auth**](https://jwt-auth.readthedocs.io) package that you may install using Composer:

```
composer require tymon/jwt-auth 1.0.0-rc.1
```

* After installing the package via Composer, you will need to publish the config file:

```
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
```

* This will create a `config/jwt.php` file which you may configure later.

* Last but not least, you will need to generate a secret key:

```
php artisan jwt:secret
```

* This will add a line to your `.env` file with the following as shown in the example below:

```
JWT_SECRET=CFNi4CpLIawEe9QiBYiUu2f6isSJu0p1
```

### Updating User Class

* To use JWT for authentication, you will need to update your `User` class to implement the `Tymon\JWTAuth\Contracts\JWTSubject` interface.

* By implementing the abovementioned interface, you are now required to define the methods `getJWTIdentifier()` and `getJWTCustomClaims()`.

* Add/modify the following lines:

```php

// ...
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
	// ...
	
	/**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
```

### Configure Auth Guard

In the `config/auth.php` file, configure Laravel to use the `jwt` guard for the `api` key, then set the default guard to `api`:

```php
'defaults' => [
    'guard' => 'api',
    'passwords' => 'users',
],

// ...

'guards' => [
    'api' => [
        'driver' => 'jwt',
        'provider' => 'users',
    ],
],
```

### Defining the Routes

* We will need to define some routes to will map to a controller to handle JWT Authentication.

* In your `routes/api.php`, define the following routes:

```php
Route::middleware('api')->namespace('Auth')->prefix('auth')->group(function() {
	Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});
```

### Implementing the Controller

* Define your controller `app/Http/Controllers/Auth/AuthController` as follows:

```php
<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
```

### Logging In

* To login, you will perform a `POST` request to the endpoint `/api/auth/login` with the following sample request body in JSON:

```
{
	"email":"chiakimhooi@email.com",
	"password":"duckypass"
}
```

* If authentication is successful, the following JSON response will be returned:

```
{
    "access_token": "eyJhbGciOiLIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lLiwiYiRtaW4iOnRydWV9.TJVA95OrM7E2cBab10RMHrHDcEfxjoYZgeFONFh7HgQ",
    "token_type": "bearer",
    "expires_in": 3600
}
```

### Making Authenticated Requests

* Now, you can make authenticated requests to protected routes by providing the token in the following ways:

#### Authorization Header

* Provide the token in your request header as follows:

```
Authorization: Bearer eyJhbGciOiLIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lLiwiYiRtaW4iOnRydWV9.TJVA95OrM7E2cBab10RMHrHDcEfxjoYZgeFONFh7HgQ
```

* As you can see above, the value for the `Authorization` header consists of the string `Bearer ` (note the space) followed by the token.

#### Query String Parameter

* Provide the token via the query string parameter on the URL. This can be useful for web requests that loads HTML page:

```
http://yourdomain.com/posts?token=eyJhbGciOiLIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lLiwiYiRtaW4iOnRydWV9.TJVA95OrM7E2cBab10RMHrHDcEfxjoYZgeFONFh7HgQ
```

---
###### The contents in this page are generally based on Laravel's Documentation at https://laravel.com/docs/5.5
</xmp> 
<script src="https://strapdownjs.com/v/0.2/strapdown.js"></script>
</body>
</html>
