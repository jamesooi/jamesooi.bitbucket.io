<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 03: Database &amp; Eloquent ORM</title>
</head>
<body>
<xmp theme="united" style="display:none;">

# Lecture 03 (B): API Resources

---

## Introduction

* Recall that in Lecture 02, you have seen that we can simply return a model (or a collection), and the output will be in JSON format as shown below:

```
{"id":1,"reg_no":"PKC7453","make":"Naza","model":"Forte","year":2011,"created_at":"2019-01-10 05:58:53","updated_at":"2019-01-10 05:58:53"}
```

* In the above JSON output, you can see that it contains every attribute of a **Vehicle** model.

* However, you may find that in many cases, you will need to transform the data before sending the output in JSON format. You may also need to exclude certain attributes (e.g. password hashes or any unnecessary attributes) or include a collection of related models.

* To do so, you will need to define **resources**.

## Resources

* The **resource** classes allow you to expressively and easily transform your **models** and **model collections** into JSON..

* To generate a resource class, use the `make:resource` Artisan command. Resources extend the `Illuminate\Http\Resources\Json\Resource` class:

```
php artisan make:resource VehicleResource
```

* By default, resources will be placed in the `app/Http/Resources` directory of your application.

* The resource file looks as shown below:

```php
<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class VehicleResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
```

### _toArray()_ Method

* Every resource class defines a `toArray()` method which returns an array of attributes that will be converted to JSON when sending the response. In the above example, all attributes will be returned in the JSON response.

* You can access model properties directly from `$this` because a resource class will automatically proxy property and method access down to the underlying model for convenient access. Hence, if you would like to omit the `created_at` and `updated_at` attributes, you may write the `toArray()` method like this:

```php
/**
 * Transform the resource into an array.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return array
 */
public function toArray($request)
{
    return [
        'id' => $this->id,
        'reg_no' => $this->reg_no,
        'make' => $this->make,
        'model' => $this->model,
        'year' => $this->year,
    ];
}
```

* You may now be return the resource from a route or controller:

```php
<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Http\Resources\VehicleResource;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    public function show($id)
    {
        $vehicle = Vehicle::find($id);

        if(!$vehicle) {
            return response()->json([
                'error' => 404,
                'message' => 'Not found'
            ], 404);
        }

        return new VehicleResource($vehicle);
    }
	
	// other methods...
}
```

* Now, if you access the API `GET /api/vehicles/1`, the JSON response will only include the array as specified in the `toArray()` method:

```
{
    "data": {
        "id": 1,
        "reg_no": "PKC7453",
        "make": "Naza",
        "model": "Forte",
        "year": 2011
    }
}
```

## Resource Collections

* To return a collection of resources, use the `collection()` method when creating the resource instance:

```php
<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Http\Resources\VehicleResource;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    public function index()
    {
        return VehicleResource::collection(Vehicle::all());
    }
	
	// other methods...
}
```

* The response returned:

```
{
    "data": [
        {
            "id": 1,
            "reg_no": "PKC7453",
            "make": "Naza",
            "model": "Forte",
            "year": 2011
        },
        {
            "id": 2,
            "reg_no": "AGP8681",
            "make": "Toyota",
            "model": "Vios",
            "year": 2008
        },
        {
            "id": 3,
            "reg_no": "PROTON1",
            "make": "Proton",
            "model": "Saga",
            "year": 1985
        }
    ]
}
```

* Sometimes, you may want to include additional metadata in your JSON response. Using the `collection()` method does not allow you to do so.

* You can define a dedicated resource to represent a collection:

```
php artisan make:resource VehicleCollection
```

* This generates a collection resource like this:

```php
<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VehicleCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
```

* You can now define additional metadata by customizing the `toArray()` method:

```php
/**
 * Transform the resource collection into an array.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return array
 */
public function toArray($request)
{
    return [
        'data' => $this->collection,
        'meta' => [
            'time' => date('U'),
        ],
    ];
}
```

* To return your collection resource:

```php
<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Http\Resources\VehicleCollection;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    public function index()
    {
        return new VehicleCollection(Vehicle::all());
    }
	
	// other methods...
}
```

* And the response is as follows:

```
{
    "data": [
        {
            "id": 1,
            "reg_no": "PKC7453",
            "make": "Naza",
            "model": "Forte",
            "year": 2011,
            "created_at": "2019-01-10 05:58:53",
            "updated_at": "2019-01-10 05:58:53"
        },
        {
            "id": 2,
            "reg_no": "AGP8681",
            "make": "Toyota",
            "model": "Vios",
            "year": 2008,
            "created_at": "2019-01-10 05:59:46",
            "updated_at": "2019-01-10 05:59:46"
        },
        {
            "id": 3,
            "reg_no": "PROTON1",
            "make": "Proton",
            "model": "Saga",
            "year": 1985,
            "created_at": "2019-01-15 02:55:39",
            "updated_at": "2019-01-15 02:55:39"
        }
    ],
    "meta": {
        "time": "1548054691"
    }
}
```

* Notice that all attributes of Vehicle models are included in the response when using a resource collection.

* To filter or hide attributes like how we want only certain attributes as declared in the `VehicleResource` resource, you need to specify your `VehicleResource` resource as follows:

```php
<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VehicleCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => VehicleResource::collection($this->collection),
            'meta' => [
                'time' => date('U'),
            ],
        ];
    }
}
```

* This will produce the following response when returned:

```
{
    "data": [
        {
            "id": 1,
            "reg_no": "PKC7453",
            "make": "Naza",
            "model": "Forte",
            "year": 2011
        },
        {
            "id": 2,
            "reg_no": "AGP8681",
            "make": "Toyota",
            "model": "Vios",
            "year": 2008
        },
        {
            "id": 3,
            "reg_no": "PROTON1",
            "make": "Proton",
            "model": "Saga",
            "year": 1985
        }
    ],
    "meta": {
        "time": "1548056204"
    }
}
```

### Pagination

* Pass a paginator instance to the collection method of a resource or to a custom resource collection to provide a paginated response.

* In the example below, we specify a paginator of *5* items per page:

```php
<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Http\Resources\VehicleCollection;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    public function index()
    {
        return new VehicleCollection(Vehicle::paginate(5));
    }
	
	// other methods...
}
```

* Returning the collection will result in a response containing links & relevant metadata:

```
{
    "data": [
        {
            "id": 1,
            "reg_no": "PKC7453",
            "make": "Naza",
            "model": "Forte",
            "year": 2011
        },
        {
            "id": 2,
            "reg_no": "AGP8681",
            "make": "Toyota",
            "model": "Vios",
            "year": 2008
        },
        {
            "id": 3,
            "reg_no": "PROTON1",
            "make": "Proton",
            "model": "Saga",
            "year": 1985
        },
        {
            "id": 4,
            "reg_no": "MALAYSIA2020",
            "make": "Proton",
            "model": "Perdana",
            "year": 2018
        },
        {
            "id": 5,
            "reg_no": "PJY6617",
            "make": "Perodua",
            "model": "Alza",
            "year": 2010
        }
    ],
    "meta": {
        "time": "1548057135",
        "current_page": 1,
        "from": 1,
        "last_page": 3,
        "path": "http://localhost:33294/api/vehicles",
        "per_page": 5,
        "to": 5,
        "total": 12
    },
    "links": {
        "first": "http://localhost:33294/api/vehicles?page=1",
        "last": "http://localhost:33294/api/vehicles?page=3",
        "prev": null,
        "next": "http://localhost:33294/api/vehicles?page=2"
    }
}
```

* Accessing the links will provide you with the response of the respective page specified, for example, if you access `http://localhost:33294/api/vehicles?page=2`:

```
{
    "data": [
        {
            "id": 6,
            "reg_no": "PLG2351",
            "make": "Proton",
            "model": "Saga",
            "year": 2015
        },
        {
            "id": 7,
            "reg_no": "ABU3527",
            "make": "Nissan",
            "model": "Sunny",
            "year": 1990
        },
        {
            "id": 8,
            "reg_no": "DR5321",
            "make": "Nissan",
            "model": "Sunny",
            "year": 1992
        },
        {
            "id": 9,
            "reg_no": "WWR7543",
            "make": "Toyota",
            "model": "Vios",
            "year": 2012
        },
        {
            "id": 10,
            "reg_no": "WUP3519",
            "make": "Honda",
            "model": "City",
            "year": 2010
        }
    ],
    "meta": {
        "time": "1548057306",
        "current_page": 2,
        "from": 6,
        "last_page": 3,
        "path": "http://localhost:33294/api/vehicles",
        "per_page": 5,
        "to": 10,
        "total": 12
    },
    "links": {
        "first": "http://localhost:33294/api/vehicles?page=1",
        "last": "http://localhost:33294/api/vehicles?page=3",
        "prev": "http://localhost:33294/api/vehicles?page=1",
        "next": "http://localhost:33294/api/vehicles?page=3"
    }
}
```

### Data Wrapping

* You would have noticed by now that when using API resources, data in JSON responses are wrapped within a key named `data`.

* To disable data wrapping, you will need to add the `Resource::withoutWrapping()` statement in the `AppServiceProvider` class as follows:

```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Resources\Json\Resource;	// ADD THIS LINE

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Resource::withoutWrapping();			// ADD THIS LINE
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

```

* Now, if you access the API `GET /api/vehicles/1`, the JSON response will not be wrapped within the `data` key:

```
{
    "id": 1,
    "reg_no": "PKC7453",
    "make": "Naza",
    "model": "Forte",
    "year": 2011
}
```

* The `withoutWrapping()` method only affects the outer-most response and will not remove data keys that you manually add to your own resource collections.

* Do take note also that when returning paginated collections in a resource response, Laravel will wrap your resource data in a `data` key even if the `withoutWrapping()` method has been called. This is because paginated responses always contain `meta` and `links` keys with information about the paginator's state.

### Conditional Attributes

* In some situations, you only want to include an attribute in a resource response if a given condition is met.

* As an example, you may want to include the attribute only if it is not null.

* Use the `when()` method to conditionally add an attribute to a resource response:

```php
public function toArray($request)
{
    return [
        'id' => $this->id,
        'reg_no' => $this->reg_no,
        'make' => $this->when(!is_null($this->make), $this->make),
        'model' => $this->when(!is_null($this->model), $this->model),
        'year' => $this->when(!is_null($this->year), $this->year),
    ];
}
```

* You can also provide a Closure as the second argument for `when()` to perform any computation before returning the value.

* For example, you may want to convert all letters to uppercase before returning:

```php
public function toArray($request)
{
    return [
        'id' => $this->id,
        'reg_no' => $this->when(!is_null($this->reg_no), function () {
            return mb_strtoupper($this->reg_no);
        }),
        'make' => $this->when(!is_null($this->make), $this->make),
        'model' => $this->when(!is_null($this->model), $this->model),
        'year' => $this->when(!is_null($this->year), $this->year),
    ];
}
```

* Or perhaps, you want to return the `make` and `model` as one key named `make_model`:

```php
public function toArray($request)
{
    return [
        'id' => $this->id,
        'reg_no' => $this->when(!is_null($this->reg_no), $this->reg_no),
        'make_model' => $this->when(!is_null($this->make) && !is_null($this->model), function() {
            return $this->make . ' ' . $this->model;
        }),
        'year' => $this->when(!is_null($this->year), $this->year),
    ];
}
```

### Conditional Relationships

* You can conditionally include relationships on your resource responses by checking if the relationship has already been loaded on the model.

* This allows you to specify in your controller which relationships should be loaded on the model and your resource can include them only when they have actually been loaded.

* Use the `whenLoaded()` method to conditionally load a relationship.

* In order to avoid unnecessarily loading relationships, the `whenLoaded()` method accepts the name of the relationship instead of the relationship itself:

```php
public function toArray($request)
{
    return [
        'id' => $this->id,
        'reg_no' => $this->when(!is_null($this->reg_no), function () {
            return mb_strtoupper($this->reg_no);
        }),
        'make' => $this->when(!is_null($this->make), $this->make),
        'model' => $this->when(!is_null($this->model), $this->model),
        'year' => $this->when(!is_null($this->year), $this->year),
        'trips' => TripResource::collection($this->whenLoaded('trips')),
    ];
}
```

* Now, if you access the API `GET /api/vehicles/1`, the JSON response will be as follows:

```
{
    "data": {
        "id": 1,
        "reg_no": "PKC7453",
        "make": "Naza",
        "model": "Forte",
        "year": 2011
    }
}
```

* You will notice that the `trips` key is not included in the JSON response. This is because the `trips` relationship is NOT loaded.

* To load the relationship, you will need to do so in the controller:

```php
public function show($id)
{
    $vehicle = Vehicle::with('trips')->find($id);

    if(!$vehicle) {
        return response()->json([
            'error' => 404,
            'message' => 'Not found'
        ], 404);
    }

    return new VehicleResource($vehicle);
}
```

* Only then, you will see a collection of `trips` in the JSON response as shown below:

```
{
    "data": {
        "id": 1,
        "reg_no": "PKC7453",
        "make": "Naza",
        "model": "Forte",
        "year": 2011,
        "trips": [
            {
                "id": 1,
                "departure_location": "George Town",
                "atd": "2019-02-01 08:00:00",
                "arrival_location": "Teluk Intan",
                "ata": "2019-02-01 11:15:00",
                "distance": 260
            },
            {
                "id": 2,
                "departure_location": "Teluk Intan",
                "atd": "2019-02-05 23:00:00",
                "arrival_location": "George Town",
                "ata": "2019-02-06 02:02:00",
                "distance": 260
            }
        ]
    }
}
```

### Conditional Pivot Information

* You can conditionally include data from the pivot tables of many-to-many relationships using the `whenPivotLoaded()` method.

* The method accepts the name of the pivot table as its first argument.

* The second argument should be a Closure that defines the value to be returned if the pivot information is available on the model:

```php
public function toArray($request)
{
    return [
        'id' => $this->id,
        'name' => $this->name,
        'expires_at' => $this->whenPivotLoaded('role_users', function () {
            return $this->pivot->expires_at;
        }),
    ];
}
```

---
###### The contents in this page are generally based on Laravel's Documentation at https://laravel.com/docs/5.5
</xmp> 
<script src="https://strapdownjs.com/v/0.2/strapdown.js"></script>
</body>
</html>
